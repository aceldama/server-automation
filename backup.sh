#!/bin/bash

#--------------------------------------------------------------------------------------------------[ CONFIG DEFAULTS ]--
VERSION="0.0.1"
UPDATE_URL="https://gitlab.com/aceldama/server-automation/raw/master/backup.sh"

BACKUP_PASS="$( tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1 )"
TIME_HH=3
TIME_MM=0

KEEP_LOCAL_FOR_N_DAYS=10

PATH_LOCAL="/backup"
PATH_REMOTE="/server-backup"
PATH_LOGFILE="/var/log/backups.log"

FTP_ENABLED=0
FTP_HOST="000.000.000.000"
FTP_USER="ftp-username"
FTP_PASS="ftp-password"


#--------------------------------------------------------------------------------------------------------[ FUNCTIONS ]--
#---------                                                                              ------------------[ MESSAGES ]--
function show_help() {
    cat << EOF
    HALP!!
    
        --backup        Performs a backup.
        
        --config        Configures all settings for the script.
        
        --help          Displays this screen.
        
        --install       Installs the backup script. If PATH is provided, it 
             < OR >     installs the script to the specified path. It will be
        --install=PATH  created if it doesn't exist.
                        
        --purge[=OPT]   Uninstalls the script and purges specified backups. The
                        options are ALL, LOCAL (default) or REMOTE.

        --restore       Restores a local or remote backup.
        
        --uninstall     Purges the script from the system. Backups will remain 
                        in the backup folder. 
        
        --update        Checks for a newer version of the script and updates 
                        if necessary.
EOF
}

function show_log() {
    echo "$1" >&2
    echo "$( date +"%Y%m%d-%H%M%S" ):  $1" >> "$PATH_LOGFILE"
}

function show_error() {
    show_log "$2"
    if [ "$1" -lt 1 ]; then
        show_help
    fi
    exit "$1"
}

function yesno() {
    local result=9
    
    while [ $result -eq 9 ]; do
        read -n1 -r -p " * [Y/n] $1: " YN
        case $YN in
            [Yy]* | "" )
                result=1
                ;;
            
            [Nn]* )
                result=0
                ;;
            * )
                echo -e "\n   '$YN'? Really? You had ONE job...  @(-__-)@" >&2
                ;;
        esac
    done
    
    return "$result"
}


#---------                                                                              -------------------[ GENERAL ]--
function do_backup() {
    #-- Pass backup:  sudo tar -zcv "/root" | openssl enc -aes-256-cbc -salt -in - -out file.enc -k "$PASSWORD"
    
    #-- Get settings and timestamp
    settings_load
    TS=$( date +"%Y%m%d-%H%M%S" )

    #-- Go to directory
    cd "$PATH_LOCAL" || show_error 3 "Backup directory missing!"

    #-- Create backup archives
    tar -zcpf "$PATH_LOCAL/$TS-full.tar.gz" --exclude="$PATH_LOCAL" --exclude="/proc" --exclude="/sys" --exclude="/dev" "/"
    tar -zcpf "$PATH_LOCAL/$TS-home.tar.gz" --exclude="$PATH_LOCAL" "/home"
    tar -zcpf "$PATH_LOCAL/$TS-root.tar.gz" --exclude="$PATH_LOCAL" "/root"


    #-- Send files to remote server
    ftp -inv $FTP_HOST << EOF
user $FTP_USER $FTP_PASS
cd $PATH_REMOTE
mput $PATH_LOCAL/$TS-*
bye
EOF


    #-- Delete local files older than N days (remote files unaffected)
    sudo find . -mtime "+$KEEP_LOCAL_FOR_N_DAYS" -type f -name "*.tar.gz" -delete
}

function do_config() {
    echo "To do..."
    exit 0
}

function do_install() {
    #-- Check/get depends or die
    apt_require "ftp"      #-- To upload to fileserver
    apt_require "openssl"  #-- To encrypt/decrypt backups
    
    #-- Create local path
    if [ ! -d "$1" ]; then
        mkdir -p "$1" || show_error 2 "Could not create the backup directory"
    fi
    PATH_LOCAL="$1"
    
    #-- Set up
    if [ -f "$PATH_LOCAL/backup.cfg" ] && ( yesno "Previous config found. Import?" ); then
        # shellcheck source=/dev/null
        source "$PATH_LOCAL/backup.cfg"
    fi
    do_config "$PATH_LOCAL"
    
    #-- Copy script
    cp "$0" "$PATH_LOCAL/backup"
    chmod +x "$PATH_LOCAL/backup"
    
    #-- Create cron job
    local tcron;
    tcron="/tmp/temp_cron"
    crontab -l | sed -in '/#backup-cron$/!p' >> "$tcron"                        #-- Get crontab and strip old jobs
    echo "$TIME_MM $TIME_HH * * * $PATH_LOCAL/backup #backup-cron" >> "$tcron"  #-- Add the new job
    crontab "$tcron"                                                            #-- Install new cron file
    rm "$tcron"                                                                 #-- Clean up
}

function do_purge() {
    echo "To do..."
    exit 0
    #cron job
    #logs
    #remote backups
    #folder and contents
}

function do_restore() {
    #-- Pass Restore: openssl enc -d -aes256 -in file.enc -k "$PASSWORD" | tar xz -C .
    echo "To do..."
    exit 0
}

function do_unininstall() {
    echo "To do..."
    exit 0
}

function do_update() {
    echo "To do..."
    exit 0
}


#---------                                                                              --------------------[ SYSTEM ]--
function need_root() {
    if ! [ "$(id -u)" -eq 0 ]; then
       echo "I am not root! >:(" >&2
       exit 1
    fi
}

function apt_require() {
    if [ "$(dpkg-query -W -f='${Status}' "$1" 2>/dev/null | grep -c "ok installed")" -eq 0 ]; then 
        show_log "  - Installing required package '$1'.";
        if ! apt -qq -y install "$1"; then
            show_error 5 "The command failed, exiting."
        else
            show_log "  - OK"
        fi
    fi
}

#---------                                                                              ------------------[ SETTINGS ]--
function settings_load() {
    local settings_file
    settings_file="$( cd "$(dirname "$0")" || exit ; pwd -P )/backup.cfg"
    
    if [ -f "$settings_file" ]; then
        # shellcheck source=/dev/null
        source "$settings_file"
    else
        show_error 6 "Settings file missing, please run --config"
    fi
}

function settings_save() {
    tee "$PATH_LOCAL/backup.cfg" << EOF
VERSION="$VERSION"
UPDATE_URL="$UPDATE_URL"

BACKUP_PASS="$BACKUP_PASS" #ENC
TIME_HH=$TIME_HH
TIME_MM=$TIME_MM

KEEP_LOCAL_FOR_N_DAYS="$KEEP_LOCAL_FOR_N_DAYS"

PATH_LOCAL="$PATH_LOCAL"
PATH_REMOTE="$PATH_REMOTE"
PATH_LOGFILE="$PATH_LOGFILE"

FTP_ENABLED=$FTP_ENABLED
FTP_HOST="$FTP_HOST" #ENC
FTP_USER="$FTP_USER" #ENC
FTP_PASS="$FTP_PASS" #ENC
EOF
}


#-------------------------------------------------------------------------------------------------------------[ MAIN ]--
if [ "$#" -eq 1 ]; then
    #-- Init
    need_root
    TS=$( date +"%Y%m%d-%H%M%S" )

    #-- Process parameters
    case "$1" in
        --backup )
            do_backup "$PATH_LOCAL"
            ;;
        
        --config )
            do_config
            ;;
        
        --help )
            show_help
            ;;
        
        --install | --install=* )
            if [ "$1" == "--install" ]; then
                #-- Use default path
                do_install "$PATH_LOCAL"
            else
                #-- Use specified path
                do-install "${1##*=}"
            fi
            ;;
        
        --purge | --purge=* )
            do_purge
            ;;
        
        --restore )
            do_restore
            ;;
        
        --uninstall )
            do_uninstall
            ;;
        
        *)
            show_error 0 "Well, that was unexpected. Parameter '$1' not recognised."

    esac
else
    #-- Show usage
    show_help
fi
